from flask import Flask, render_template
import requests, random, json

app = Flask(__name__)

def getDinos():
    return requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/').json()

def getDinosData(dinoSlug):
    return requests.get("https://medusa.delahayeyourself.info/api/dinosaurs/%s" %dinoSlug).json()

def getRandomDinos():
    return random.sample(getDinos(), 3)



@app.route('/')
def index():
    return render_template("liste.html", listDino=getDinos())

@app.route('/dinosaur/<slug>/')
def data_dino(slug):
    return render_template('dinosaures.html', dino=getDinosData(slug), randomDinos=getRandomDinos())